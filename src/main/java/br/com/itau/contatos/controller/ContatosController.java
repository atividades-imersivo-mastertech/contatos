package br.com.itau.contatos.controller;

import br.com.itau.contatos.models.Contato;
import br.com.itau.contatos.models.Usuario;
import br.com.itau.contatos.models.dto.ContatoMapper;
import br.com.itau.contatos.models.dto.request.ContatoRequest;
import br.com.itau.contatos.models.dto.response.ContatoResponse;
import br.com.itau.contatos.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;


import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/contato")
public class ContatosController {

    @Autowired
    private ContatoService contatoService;

    @Autowired
    private ContatoMapper mapper;

    @GetMapping("/lista")
    public List<ContatoResponse> getContatos(@AuthenticationPrincipal Usuario user) {
        List<Contato> contatos = contatoService.listarPorUsuario(user);
        List<ContatoResponse> conatosResponse = new ArrayList<>();
        contatos.forEach(contato -> conatosResponse.add(mapper.fromContato(contato)));

        return conatosResponse;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ContatoResponse novoContato(@RequestBody ContatoRequest contatoRequest, @AuthenticationPrincipal Usuario user) {
        Contato contato = mapper.toContato(contatoRequest);
        contato.setIdProprietario(user.getId());
        return mapper.fromContato(contatoService.salvar(contato));
    }

}

