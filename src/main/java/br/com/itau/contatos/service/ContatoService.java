package br.com.itau.contatos.service;

import br.com.itau.contatos.models.Contato;
import br.com.itau.contatos.models.Usuario;
import br.com.itau.contatos.repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato salvar(Contato contato) {
        return contatoRepository.save(contato);
    }

    public List<Contato> listarPorUsuario (Usuario user){
        return contatoRepository.findByIdProprietario(user.getId());
    }
}
