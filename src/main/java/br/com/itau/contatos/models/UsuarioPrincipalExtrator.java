package br.com.itau.contatos.models;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

import java.util.Map;

public class UsuarioPrincipalExtrator implements PrincipalExtractor {
    @Override
    public Object extractPrincipal(Map<String, Object> map) {
        Usuario usuario = new Usuario();
        usuario.setId(Long.parseLong(map.get("id").toString()));
        usuario.setName((String) map.get("name"));
        return usuario;
    }
}
