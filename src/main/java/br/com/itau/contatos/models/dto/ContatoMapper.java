package br.com.itau.contatos.models.dto;

import br.com.itau.contatos.models.Contato;
import br.com.itau.contatos.models.dto.request.ContatoRequest;
import br.com.itau.contatos.models.dto.response.ContatoResponse;
import org.springframework.stereotype.Component;

@Component
public class ContatoMapper {

    public Contato toContato(ContatoRequest contatoRequest){
        Contato contato = new Contato();
        contato.setNome(contatoRequest.getNome());
        contato.setNumeroTelefone(contatoRequest.getNumeroTelefone());
        return contato;
    }

    public ContatoResponse fromContato(Contato contato) {
        ContatoResponse contatoResponse = new ContatoResponse();

        contatoResponse.setId(contato.getId());
        contatoResponse.setNome(contato.getNome());
        contatoResponse.setNumeroTelefone(contato.getNumeroTelefone());

        return contatoResponse;
    }

}
