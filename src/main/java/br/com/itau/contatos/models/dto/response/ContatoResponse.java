package br.com.itau.contatos.models.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContatoResponse {
    @JsonProperty(value = "id_contato")
    private Long id;

    @JsonProperty(value = "nome")
    private String nome;

    @JsonProperty(value = "telefone")
    private String numeroTelefone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumeroTelefone() {
        return numeroTelefone;
    }

    public void setNumeroTelefone(String numeroTelefone) {
        this.numeroTelefone = numeroTelefone;
    }
}
