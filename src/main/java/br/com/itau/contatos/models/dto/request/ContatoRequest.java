package br.com.itau.contatos.models.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContatoRequest {
    @JsonProperty(value = "nome")
    private String nome;

    @JsonProperty(value = "telefone")
    private String numeroTelefone;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumeroTelefone() {
        return numeroTelefone;
    }

    public void setNumeroTelefone(String numeroTelefone) {
        this.numeroTelefone = numeroTelefone;
    }
}
